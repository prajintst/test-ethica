// grab the packages we need
var express = require('express');
var app = express();
var port = process.env.PORT || 8080;
var fs = require("fs");
// routes will go here

// start the server
app.listen(port);
console.log('Server started! At http://localhost:' + port);
app.set('view engine', 'pug');

app.get('/user/edit/get', function (req, res) {
    var jobId = req.param('jobId');
    var fieldId = req.param('fieldId');
    var updateValue = req.param('updateValue');
    if (updateValue) {
        
        writeFile("FIELD_ID.txt",fieldId);
        writeFile("UPDATE_VALUE.txt", updateValue);
        writeFile("JOB_ID.txt",jobId);
        res.send({jobId, fieldId ,updateValue});
    } else {
        res.render('send', {
            jobId,
            fieldId
        });
    }



});

function writeFile(filename, text) {

    fs.writeFile(filename, text, function (err) {

        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });

}